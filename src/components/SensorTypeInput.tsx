import React, { useState } from 'react';
import config from '../shared/config';
interface SensorTypeResponse {
  type: string;
  id: string;
}

async function createSensorType(type: string): Promise<SensorTypeResponse | null> {
  try {
    const response = await fetch(config.api_url + '/api/sensortype', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ type }),
    });

    if (!response.ok) {
      throw new Error('Error creating sensor type');
    }

    const data: SensorTypeResponse = await response.json();
    return data;
  } catch (error) {
    console.error('Error:', error);
    return null;
  }
}

const SensorTypeInput: React.FC = () =>{
    const [sensorType, setSensorType] = useState('');
    const [result, setResult] = useState<SensorTypeResponse | null>(null);
  
    const handleSubmit = async (event: React.FormEvent) => {
      event.preventDefault();
  
      if (!sensorType) {
        return;
      }
  
      const data = await createSensorType(sensorType);
  
      if (data) {
        setResult(data);
        setSensorType('');
      }
    };
  
    return (
      <div>
        <h2>Create Sensor Type</h2>
        <form onSubmit={handleSubmit}>
          <label>
            Sensor Type:
            <input
              type="text"
              value={sensorType}
              onChange={(event) => setSensorType(event.target.value)}
            />
          </label>
          <button type="submit">Submit</button>
        </form>
        {result && (
          <p>
            Created sensor type {result.type} with ID {result.id}
          </p>
        )}
      </div>
    );
  };
  
  export default SensorTypeInput;