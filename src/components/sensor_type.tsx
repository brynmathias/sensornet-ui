
import React, { useEffect, useState } from 'react';
import fetchData from '../shared/fetchData';
import config from '../shared/config';
  interface SensorObject {
    // Add the properties of your SensorObject here.
    // For example:
    uuid: string,
    type: string,
    name: string,
    storage_path: string,
    rtsp_url: string,
    source_type: string 

    // details: string;
    // ...
  }

  async function fetchApiData(): Promise<SensorObject[]> {
    return fetchData<SensorObject>(config.api_url + '/api/sensor', (jsonData) => {
      if (Array.isArray(jsonData.Sensors)) {
        console.log("got a list of sesnsors")
        return jsonData.Sensors.map((item: any) => ({
            uuid: item.uuid,
            type: item.type,
            name: item.sensor_name,
            storage_path: item.storage_path,
            rtsp_url: item.source_fqdn,
            source_type: item.source_type,
            geo_lat_lon: item.geo_lat_lon || [], // Set geo_lat_lon to an empty array if it's null
          }));
     }
  
        console.log("got No sensors")
      return [];
    });
  }

    
    const SensorList: React.FC = () => {
        const [sensorObjects, setSensorObjects] = useState<SensorObject[]>([]);

        useEffect(() => {
          const fetchData = async () => {
            const data = await fetchApiData();
            setSensorObjects(data);
          };
      
          fetchData();
        }, []);
      
        return (
            <div>
              <h1>Sensor Objects</h1>
              <ul>
                {sensorObjects.map((sensor) => (
                  <li key={sensor.uuid}>
                    {sensor.name} ({sensor.type}): {sensor.source_type} <br />
                    Storage Path: {sensor.storage_path} <br />
                    RTSP URL: <a href={sensor.rtsp_url}>{sensor.rtsp_url}</a>
                  </li>
                ))}
              </ul>
            </div>
          );
        };      
      export default SensorList; 