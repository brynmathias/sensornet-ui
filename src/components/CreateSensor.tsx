import React, { useEffect, useState } from 'react';
import fetchData from '../shared/fetchData';
import config from '../shared/config';
interface SensorType {
  type: string;
  id: string;
}

interface SensorDetails {
  uuid: string;
  sensor_type: string;
  storage_path: string;
  geo_lat_lon: number[];
  source_fqdn: string;
  source_type: string;
}

async function fetchSensorTypes(): Promise<SensorType[]> {
    try {
      const response = await fetch(config.api_url + "/api/sensortype");
      const jsonData = await response.json();
  
      // Check if the API response contains the sensor_types array
      if (!Array.isArray(jsonData.sensor_types)) {
        console.error('Unexpected API response:', jsonData);
        return [];
      }
  
      return jsonData.sensor_types;
    } catch (error) {
      console.error('Error fetching sensor types:', error);
      return [];
    }
  }



async function createSensor(sensorDetails: Omit<SensorDetails, 'uuid'>): Promise<SensorDetails | null> {
    try {
      const response = await fetch(config.api_url + '/api/sensor', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(sensorDetails),
      });
  
      if (!response.ok) {
        throw new Error('Error creating sensor');
      }
  
      const data: SensorDetails = await response.json();
      return data;
    } catch (error) {
      console.error('Error:', error);
      return null;
    }
  }
  
  const CreateSensor: React.FC = () => {
    const [sensorTypes, setSensorTypes] = useState<SensorType[]>([]);
    const [selectedType, setSelectedType] = useState('');
    const [storagePath, setStoragePath] = useState('');
    const [geoLatLon, setGeoLatLon] = useState('');
    const [sourceFqdn, setSourceFqdn] = useState('');
    const [sourceType, setSourceType] = useState('');
    const [result, setResult] = useState<SensorDetails | null>(null);
  
    useEffect(() => {
      const fetchTypes = async () => {
        const data = await fetchSensorTypes();
        setSensorTypes(data);
        if (data.length > 0) {
          setSelectedType(data[0].type);
        }
      };
  
      fetchTypes();
    }, []);
  
    const handleSubmit = async (event: React.FormEvent) => {
      event.preventDefault();
  
      const sensorDetails: Omit<SensorDetails, 'uuid'> = {
        sensor_type: selectedType,
        storage_path: storagePath,
        geo_lat_lon: geoLatLon.split(',').map(Number),
        source_fqdn: sourceFqdn,
        source_type: sourceType,
      };
  
      const data = await createSensor(sensorDetails);
  
      if (data) {
        setResult(data);
      }
    };
  
    return (
      <div>
        <h2>Create Sensor</h2>
        <form onSubmit={handleSubmit}>
          <label>
            Sensor Type:
            <select
              value={selectedType}
              onChange={(event) => setSelectedType(event.target.value)}
            >
              {sensorTypes.map((sensorType) => (
                <option key={sensorType.id} value={sensorType.type}>
                  {sensorType.type}
                </option>
              ))}
            </select>
          </label>
          <br />
          <label>
          Storage Path:
          <input
            type="text"
            value={storagePath}
            onChange={(event) => setStoragePath(event.target.value)}
          />
        </label>
        <br />
        <label>
          Geo Lat/Lon (comma-separated):
          <input
            type="text"
            value={geoLatLon}
            onChange={(event) => setGeoLatLon(event.target.value)}
          />
        </label>
        <br />
        <label>
          Source FQDN:
          <input
            type="text"
            value={sourceFqdn}
            onChange={(event) => setSourceFqdn(event.target.value)}
          />
        </label>
        <br />
        <label>
          Source Type:
          <input
            type="text"
            value={sourceType}
            onChange={(event) => setSourceType(event.target.value)}
          />
        </label>
        <br />
        <button type="submit" onClick={handleSubmit}>Submit</button>
      </form>
      {result && (
        <p>
          Created sensor {result.sensor_type} with UUID {result.uuid}
        </p>
      )}
    </div>
  );
};

export default CreateSensor;