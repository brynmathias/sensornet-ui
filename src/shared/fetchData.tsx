
    async function fetchData<T>(
      url: string,
      processData: (jsonData: any) => T[]
    ): Promise<T[]> {
      try {
        const response = await fetch(url);
        const jsonData = await response.json();
    
        // Check if the API response contains the expected data structure
        if (Array.isArray(jsonData.Sensors)) {
          // Process the JSON data using the provided processData function
          const data: T[] = processData(jsonData);
          return data;
        } else {
          console.error('Unexpected API response:', jsonData);
          return [];
        }
      } catch (error) {
        console.error('Error fetching data:', error);
        return [];
      }
    }
    
    export default fetchData;