import * as React from "react";
import * as ReactDOM from "react-dom";
import SensorList from './components/sensor_type';
import SensorTypeInput from './components/SensorTypeInput';
import CreateSensor from './components/CreateSensor';

ReactDOM.render(
  <div>
    <h1>Hello, Welcome to SensorNet</h1>
    <SensorTypeInput />
    <CreateSensor />
    <SensorList />
  </div>,
  document.getElementById("root")
);