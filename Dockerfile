FROM node:14 AS build
ARG SENSORNET_API_URL ""
WORKDIR /app

COPY package*.json ./
RUN npm install
RUN npm ci

COPY . .

RUN npm run magic 

# Stage 2: Set up Nginx to serve the built files
FROM nginx:stable-alpine

COPY --from=build /app/dist /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]